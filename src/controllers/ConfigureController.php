<?php

namespace shfx17\zohocrmapi\controllers;

use Craft;
use craft\web\Controller;
use myprojecttests\ExampleFunctionalCest;
use shfx17\zohocrmapi\models\ZohoApiLogsModel;
use shfx17\zohocrmapi\Plugin;

use craft\web\View;

class ConfigureController extends Controller
{
    public function actionIndex() {

        if(!isset($_GET['page'])) {
            $page = 1;
        } else {
            $page = $_GET['page'];
        }

        $results_per_page = 10;

        $logs = (new \craft\db\Query())
            ->select(['id'])
            ->from('zoho_api_logs')
            ->count();

        //ile stron
        $number_of_pages = ceil($logs/$results_per_page);

        $result = ($page-1) * $results_per_page;

        $logs_limit = (new \craft\db\Query())
            ->select(['id', 'ml_number', 'craft_id', 'result_zoho', 'errors_zoho', 'action', 'dateCreated'])
            ->from('zoho_api_logs')
            ->orderBy([
                'id' => SORT_DESC
            ])
            ->limit($results_per_page)->offset($result)
            ->all();
//
        return $this->renderTemplate('zoho-crm-api/configure/index', ['logs_limit' => $logs_limit, 'number_of_pages' => $number_of_pages]);

    }

    public function actionSend() {
        $request = \Craft::$app->getRequest();

        $token = $request->getRequiredParam('token');

        if($token == Plugin::getInstance()->getSettings()->token_crm) {} else {
            if($token == '') {} else {
                $plugin = Craft::$app->getPlugins()->getPlugin('zoho-crm-api');
                Craft::$app->getPlugins()->savePluginSettings($plugin, ['token_crm' => "$token"]);

                $client = new \GuzzleHttp\Client([
                    'base_uri' => 'https://accounts.zoho.eu',
                    'verify' => false
                ], false);

                $response = $client->POST('/oauth/v2/token', [
                    'query' => [
                        'code' => "$token",
                        'redirect_url' => 'http://example.com/callbackurl',
                        'client_id' => ''.Plugin::getInstance()->getSettings()->client_id.'',
                        'client_secret' => ''.Plugin::getInstance()->getSettings()->client_secret.'',
                        'grant_type' => 'authorization_code'
                    ]
                ]);

                $json = json_decode($response->getBody(), true);

                if(!empty($json['refresh_token'])) {
                    $zoho_token =  $json['refresh_token'];

                    $plugin = Craft::$app->getPlugins()->getPlugin('zoho-crm-api');
                    Craft::$app->getPlugins()->savePluginSettings($plugin, ['zoho_token' => "$zoho_token"]);
                }
            }
        }
    }

    public function actionSave() {
        $request = \Craft::$app->getRequest();

        $client_id = $request->getRequiredParam('client_id');
        $client_secret = $request->getRequiredParam('client_secret');

        if($client_id == '' OR $client_secret == '') {} else {
            $plugin = Craft::$app->getPlugins()->getPlugin('zoho-crm-api');
            Craft::$app->getPlugins()->savePluginSettings($plugin, ['client_id' => "$client_id"]);
            Craft::$app->getPlugins()->savePluginSettings($plugin, ['client_secret' => "$client_secret"]);
        }
    }

    public function actionAuth() {
        $request = \Craft::$app->getRequest();

        $graphql_token = $request->getRequiredParam('graphql_token');

        if($graphql_token == '') {} else {
            $plugin = Craft::$app->getPlugins()->getPlugin('zoho-crm-api');
            Craft::$app->getPlugins()->savePluginSettings($plugin, ['graphql_token' => "$graphql_token"]);
        }
    }

    public function actionUrl() {

        $request = \Craft::$app->getRequest();

        $link_to_api_craftCMS = $request->getRequiredParam('link_to_api_craftCMS');

        if($link_to_api_craftCMS == '') {} else {
            $plugin = Craft::$app->getPlugins()->getPlugin('zoho-crm-api');
            Craft::$app->getPlugins()->savePluginSettings($plugin, ['link_to_api_craftCMS' => "$link_to_api_craftCMS"]);
        }
    }

    public function actionDelete() {
        \Craft::$app->db->createCommand()->delete('zoho_api_logs')->execute();

        $plugin = Craft::$app->getPlugins()->getPlugin('zoho-crm-api');
        Craft::$app->getPlugins()->savePluginSettings($plugin, ['logs_plugin' => '']);
    }
}