<?php
namespace shfx17\zohocrmapi\models;

use craft\base\Model;

class SettingsModel extends Model
{
    public $zohoCrmApiEnabled = true;

    public $markdownPole = '';

    //pierwszy token z ZohoCRM - 10 min
    public $token_crm = '';

    //drugi token tzw RefreshToken
    public $zoho_token = '';

    //ostatni token do odświeżania co 60 min
    public $access_token = '';

    //godzina pobrania danych z Zoho
    public $last_updated = '';

    //client id/
    public $client_id = '';

    //client secret
    public $client_secret = '';

    //grpahql token
    public $graphql_token = '';

    //link to api in CraftCMS
    public $link_to_api_craftCMS = '';

    //when was last update from ZohoCRM
    public $last_updated_from_zoho = '';

    //logs
    public $logs_plugin;

    //offset
    public $offset = '';
}