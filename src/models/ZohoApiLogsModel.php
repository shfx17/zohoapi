<?php

namespace shfx17\zohocrmapi\models;

use craft\db\ActiveRecord;

class ZohoApiLogsModel extends ActiveRecord
{
    public static function tableName() {
        return 'zoho_api_logs';
    }
}
