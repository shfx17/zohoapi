<?php

namespace shfx17\zohocrmapi\commands\controllers;

use Craft;
use craft\console\Controller;
use shfx17\zohocrmapi\Plugin;
use GuzzleHttp\Client;


class TokenController extends Controller
{
    public function actionRefresh() {
        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://accounts.zoho.eu',
            'verify' => false
        ], false);

        $zoho_token = Plugin::getInstance()->getSettings()->zoho_token;

        $response = $client->POST('/oauth/v2/token', [
            'query' => [
                'refresh_token' => "$zoho_token",
                'client_id' => ''.Plugin::getInstance()->getSettings()->client_id.'',
                'client_secret' => ''.Plugin::getInstance()->getSettings()->client_secret.'',
                'grant_type' => 'refresh_token'
            ]
        ]);

        $json = json_decode($response->getBody(), true);

        $access_token = $json['access_token'];

        $plugin = Craft::$app->getPlugins()->getPlugin('zoho-crm-api');
        Craft::$app->getPlugins()->savePluginSettings($plugin, ['access_token' => "$access_token"]);
    }
}