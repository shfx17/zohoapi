<?php

namespace shfx17\zohocrmapi\commands\controllers;

use Craft;
use craft\console\Controller;
use craft\fields\Categories;
use craft\models\CategoryGroup;
use craft\records\Category;
use craft\records\Element;
use craft\records\Field;
use shfx17\zohocrmapi\models\ZohoApiLogsModel;
use shfx17\zohocrmapi\Plugin;
use GuzzleHttp\Client;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use phpDocumentor\Reflection\Types\Boolean;
use Stripe\Customer;
use Stripe\Terminal\Location;
use yii\helpers\VarDumper;
use yii\base\Event;

class ZohoController extends Controller
{
    public $dataZoho;
    public $t, $i;

    public $tView = [];
    public $tSpecifications = [];
    public $tLocation = [];
    public $tPropertyType, $tSalesStatus;

    public $sum; //ilosc wyciagnietych rekordow z ZohoCRM
    public $reducedPrice, $directProperty;

    public $admin_id;

    public $dataGraphQL;

    //token wzięty z GraphQl -> Tokens -> app
    public $token;

    //mapowanie z ZOHO do CraftCMS
    public $mapSaleStatus = [
        'On the market'  => 'forSale',
        'Reserved' => 'reserved',
        'Comming on the market' => 'commingOnTheMarket',
        'Sold' => 'sold'
    ];

    public $mapNewBuild = [
        '1' => 13374,
        '0' => 13375
    ];

    public function getAdmin() {
        $rows = (new \craft\db\Query())
            ->select(['id'])
            ->from('users')
            ->where(['admin' => 1])
            ->limit(1)
            ->all();

        $data = $rows[0];
        $this->admin_id = $data['id'];
    }

    public function actionCity($city) {
        $el = Element::find()
            ->select(['content.title','elements.id'])
            ->leftJoin('content','content.elementId = elements.id')
            ->andWhere(['type' => 'craft\elements\Category'])
            ->andWhere(['content.title' => $city])
            ->asArray()
            ->all();

        $data = $el[0] ?? '';
        $this->tLocation[0] = (int)$data['id'];
    }

    public function actionArea($area) {
        $el = Element::find()
            ->select(['content.title', 'elements.id'])
            ->leftJoin('content', 'content.elementId = elements.id')
            ->andWhere(['type' => 'craft\elements\Category'])
            ->andWhere(['content.title' => $area])
            ->asArray()
            ->all();

        if(!empty($el[0])){
            $data = $el[0];
            $this->tLocation[1] = (int)$data['id'];
        }
    }

    public function actionView($view) {
        $el = Element::find()
            ->select(['content.title', 'elements.id'])
            ->leftJoin('content', 'content.elementId = elements.id')
            ->andWhere(['type' => 'craft\elements\Category'])
            ->andWhere(['content.title' => $view])
            ->asArray()
            ->all();

        $data = $el[0] ?? NULL;
        $this->tView[] = (int)$data['id'];
    }

    public function actionSpecifications($specifications) {
        $el = Element::find()
            ->select(['content.title', 'elements.id'])
            ->leftJoin('content', 'content.elementId = elements.id')
            ->andWhere(['type' => 'craft\elements\Entry'])
            ->andWhere(['content.title' => $specifications])
            ->asArray()
            ->all();

        $data = $el[0] ?? NULL;
        $this->tSpecifications[] = (int)$data['id'];
    }

    public function actionPropertyType($propertyType){
        $el = Field::find()
            ->select(['settings'])
            ->andWhere(['type' => 'craft\fields\Dropdown'])
            ->andWhere(['handle' => 'propertyType'])
            ->asArray()
            ->all();

        $data = $el[0];
        $json = json_decode($data['settings'], TRUE);

        $return = NULL;

        foreach($json['options'] as $option) {

            if($option['label'] === $propertyType) {
                $return = $option['value'];
            }
        }
        $this->tPropertyType = $return;

        // złe nazwy w CraftCMS, np. w ZohoCRM Appartment a w CraftCMS Apartment, w Zoho "Renovation" w Craft "To renovate", reszta git
    }

    public function actionSalesStatus($salesStatus) {
        $el = Field::find()
            ->select(['settings'])
            ->andWhere(['type' => 'craft\fields\Dropdown'])
            ->andWhere(['handle' => 'salesStatus'])
            ->asArray()
            ->all();

        $data = $el[0];
        $json = json_decode($data['settings'], TRUE);

        $return = NULL;

        foreach($json['options'] as $option) {

            if($option['label'] === $salesStatus) {
                $return = $option['value'];
            }
        }
        $this->tSalesStatus = $return;
    }

    public function actionImport()
    {
        $filename = './config/zohocrmapi.php';

        if(file_exists($filename)) {

            $this->getAdmin();

            $this->token = Plugin::getInstance()->getSettings()->graphql_token;

            $this->getRecords();

            $date_to_twig = date("d-m-Y H:i:s");

            $plugin = Craft::$app->getPlugins()->getPlugin('zoho-crm-api');
            Craft::$app->getPlugins()->savePluginSettings($plugin, ['last_updated' => "$date_to_twig"]);

            if (Plugin::getInstance()->getSettings()->offset == '') {
                $plugin = Craft::$app->getPlugins()->getPlugin('zoho-crm-api');
                Craft::$app->getPlugins()->savePluginSettings($plugin, ['offset' => 0]);
            }


            $this->i = Plugin::getInstance()->getSettings()->offset;

            for ($this->i; $this->i < $this->sum; $this->i++) {
                if ($this->i >= $this->sum) {
                } else {
                    $this->checkGQL($this->t[$this->i]['ML_Number']);
                }
            }
        } else {
            $this->actionTable(0, 0, 0, 'File zohocrmapi.php does not exist', 'file');
        }
    }

    public function actionTable($ml_number, $craft_id, $result_zoho, $errors_zoho, $action) {
        $log = new ZohoApiLogsModel();
        $log->ml_number = $ml_number;
        $log->craft_id = $craft_id;
        $log->result_zoho = $result_zoho;
        $log->errors_zoho = $errors_zoho;
        $log->action = $action;
        $log->save();
    }

    public function actionOffset() {
        $plugin = Craft::$app->getPlugins()->getPlugin('zoho-crm-api');
        Craft::$app->getPlugins()->savePluginSettings($plugin, ['offset' => 0]);
    }

    public function getRecords() {
        $last_updated_from_zoho = Plugin::getInstance()->getSettings()->last_updated_from_zoho;

        //echo Plugin::getInstance()->getSettings()->last_updated_from_zoho;

        if($last_updated_from_zoho == '') {
            $last_updated_from_zoho = '2021-03-22T12:00:00+02:00';
        }

        $access_token = Plugin::getInstance()->getSettings()->access_token;

        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://zohoapis.eu',
            'verify' => FALSE,
            'headers' => [
                'Authorization' => 'Zoho-oauthtoken ' . "$access_token",
                'If-Modified-Since' => "$last_updated_from_zoho"
            ]
        ], FALSE);

        $response = $client->GET('/crm/v2/Properties');

        $json = json_decode($response->getBody(), TRUE);

        if(!empty($json)){
            $this->dataZoho = $json['data'];
            //echo $json['data'];
            //var_dump($json);
            $this->t = $json['data'];

            foreach($json['data'] as $data) {
                $this->sum += 1;
            }

            $current_date = date("Y");

            $first_day_winter = "$current_date-12-21";
            $expire = $last_updated_from_zoho;

            $first_day_winter_time = strtotime($first_day_winter);
            $expire_time = strtotime($expire);

            if($expire_time < $first_day_winter_time) {
                $dateX = date("Y-m-d\TH:i:s\+02:00");
            } else {
                $dateX = date("Y-m-d\TH:i:s\+01:00");
            }

            $plugin = Craft::$app->getPlugins()->getPlugin('zoho-crm-api');
            Craft::$app->getPlugins()->savePluginSettings($plugin, ['last_updated_from_zoho' => "$dateX"]);

        } else {
            echo 'Nothing to adding and editing';
        }
    }

    public function checkGQL($referenceNumber) {
        $start_time = time();

        $query = 'query{
          entries (section: "property", referenceNumber: '. $referenceNumber .'){
            id
            ... on property_properties_Entry{
              referenceNumber
            }
          }
        }
        ';

        $graphqlEndpoint = Plugin::getInstance()->getSettings()->link_to_api_craftCMS;
        $client = new \GuzzleHttp\Client();

        $response = $client->request('POST', $graphqlEndpoint, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->token
            ],
            'json' => [
                'query' => $query
            ]
        ]);

        $json = $response->getBody()->getContents();
        $body = json_decode($json, TRUE);

        $data = $body['data'];
        $this->dataGraphQL = $data['entries'];

        if(!isset($this->dataGraphQL[0]['id'])){
            $this->actionGraphql('', $this->i);
        } else {
            $this->actionGraphql($this->dataGraphQL[0]['id'], $this->i);
        }

        $end_time = time();
        $result = $end_time - $start_time;

        echo $result . "s " . $referenceNumber . "\n";
    }

    public function actionSettings($id) {

        foreach($this->dataZoho[$id]['Views'] as $view){
            if(isset($this->mapView[$view])){
                $this->actionView($view);
            }
        }

        foreach($this->dataZoho[$id]['Features'] as $feature){
            if(isset($this->mapSpecifications[$feature])){
                $this->actionSpecifications($feature);
            }
        }

        $this->actionCity($this->dataZoho[$id]['City'] ?? NULL);
        $this->actionArea($this->dataZoho[$id]['Area'] ?? NULL);

        if(!empty($this->dataZoho[$id]['New_reduced_price'])) {
            $this->reducedPrice = TRUE;
        } else {
            $this->reducedPrice = FALSE;
        }

        if(!empty($this->dataZoho[$id]['Direct_property'])) {
            $this->directProperty = TRUE;
        } else {
            $this->directProperty = FALSE;
        }
    }

    public function actionGraphql($id, $idpost) {

        $config = include('./config/zohocrmapi.php');

        $string = '';
        $string_variables = '';
        $string_type = '';

        foreach ($config as $item) {
            $string .= $item['craft_key'] . "\n";
            $string_variables .= $item['craft_key'] . ': $' . $item['craft_key'] . ' ' . "\n";
            $string_type .= '$' . $item['craft_key'] . ': ' . $item['type'] . ', ' . "\n";
        }
        $query = '
            mutation saveEntry('. ($id ? '$id: ID,' : '') .'$authorId: ID, '. $string_type .' $salesStatus: String, $newBuild: [Int], $propertyType: String, $view: [Int], $specifications: [Int], $location: [Int]) {
              save_property_properties_Entry(
                '. ($id ? 'id: $id' : '') .'
                authorId: $authorId
                '. $string_variables .'
                salesStatus: $salesStatus
                newBuild: $newBuild
                propertyType: $propertyType
                view: $view
                specifications: $specifications
                location: $location
                ) {
                    '. ($id ? 'id' : '') .'
                    '. $string .'
                    salesStatus
                    newBuild {
                      id
                    }
                    propertyType
                    view {
                      id
                    }
                    specifications {
                      id
                    }
                    location {
                      id
                    }
              }
            }
        ';

        $graphqlEndpoint = Plugin::getInstance()->getSettings()->link_to_api_craftCMS;
        $client = new \GuzzleHttp\Client();

        $this->actionSettings($idpost);
        $this->actionPropertyType($this->dataZoho[$idpost]['Property_Type']);
        $this->actionSalesStatus($this->dataZoho[$idpost]['Sale_Status']);

        //echo $config[1]['zoho_key'];
        /*
                $variables = [
                    'authorId' => $this->admin_id,
                    'title' => $this->dataZoho[$idpost]['Name'],
                    'handwrittenTitle' => $this->dataZoho[$idpost]['Area'],
                    'referenceNumber' => $this->dataZoho[$idpost]['ML_Number'],
                    'price' => $this->dataZoho[$idpost]['Price'],
                    'reducedPrice' => $this->reducedPrice,
                    'newPrice' => $this->dataZoho[$idpost]['New_reduced_price'],
                    'salesStatus' => $this->tSalesStatus ?? NULL,
                    'saleDate' => $this->dataZoho[$idpost]['Date_sold'],
                    'newBuild' => $this->mapNewBuild[$this->dataZoho[$idpost]['New_build']] ?? NULL,
                    'secretProperty' => $this->dataZoho[$idpost]['Secret_property'] ?? FALSE,
                    'propertyType' =>  $this->tPropertyType ?? NULL,
                    'renovationProject' => $this->directProperty,
                    'amountOfBedrooms' => $this->dataZoho[$idpost]['Bedrooms2'] ?? 0,
                    'amountOfBathrooms' => $this->dataZoho[$idpost]['Bathrooms'] ?? 0,
                    'livingAreaMeasure' => $this->dataZoho[$idpost]['Living_area_M2'],
                    'plotMeasure' => $this->dataZoho[$idpost]['Plot_measure_M2'],
                    'view' => $this->tView ?? NULL,
                    'specifications' => $this->tSpecifications ?? NULL,
                    'location' => $this->tLocation ?? NULL,
                    //'propertyImages' => [182061],
                    //'pdfFileProperty' => [],
                    'youtubeLinkProperty' => NULL,
                    'stillYoutubeVideo' => [],
                    'introText' => 'test',
                    'plotNumber' => 'z',
                    'seoContentstore' => $this->dataZoho[$idpost]['Short_description']
                ];
        */
        $variables = [
            'authorId' => $this->admin_id,
            'location' => $this->tLocation ?? NULL,
            'newBuild' => $this->mapNewBuild[$this->dataZoho[$idpost]['New_build']] ?? NULL,
            'view' => $this->tView ?? NULL,
            'propertyType' =>  $this->tPropertyType ?? NULL,
            'specifications' => $this->tSpecifications ?? NULL,
            'salesStatus' => $this->tSalesStatus ?? NULL,
        ];

        foreach ($config as $item) {
            $variables[$item['craft_key']] = $this->dataZoho[$idpost][$item['zoho_key']];
        }

        if(!empty($id)) {
            $variables['id'] = $id;
        } else {
            $id = 0;
        }

        $response = $client->request('POST', $graphqlEndpoint, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->token
            ],
            'json' => [
                'query' => $query,
                'variables' => $variables
            ]
        ]);

        $json = $response->getBody()->getContents();
        $body = json_decode($json, TRUE);

        if(isset($body['errors'])) {
            $data = $body['errors'];
            $error = $data[0]['message'];

            $true = FALSE;
        } else {
            $true = TRUE;
        }
        //$ml_number, $craft_id, $result_zoho, $errors_zoho, $action
        $this->actionTable($this->dataZoho[$idpost]['ML_Number'], $id, $true, ($error ?? 'No errors'), ($id ? 'edit' : 'create'));
    }

    public function actionTestx() {
        echo Plugin::getInstance()->getSettings()->last_updated_from_zoho;
        //$plugin = Craft::$app->getPlugins()->getPlugin('zoho-crm-api');
        //Craft::$app->getPlugins()->savePluginSettings($plugin, ['last_updated_from_zoho' => '2022-03-30T12:13:00+02:00']);
    }

}