<?php
namespace shfx17\zohocrmapi;

use Craft;
use craft\events\RegisterUrlRulesEvent;
use craft\web\UrlManager;
use shfx17\zohocrmapi\models\SettingsModel;
use yii\base\Event;

class Plugin extends \craft\base\Plugin
{
    public $hasCpSection = true;

    public $hasCpSettings = true;

    public function init()
    {
        $this->routes();
        $settings = $this->getSettings();
        $this->hasCpSection = $settings->zohoCrmApiEnabled;

        if (Craft::$app->getRequest()->getIsConsoleRequest()) {
            $this->controllerNamespace = 'shfx17\\zohocrmapi\\commands\\controllers';
        } else {
            $this->controllerNamespace = 'shfx17\\zohocrmapi\\controllers';
        }
    }

    protected function createSettingsModel()
    {
        return new SettingsModel();
    }

    protected function settingsHtml()
    {
        /*
        $logs_plugin = Plugin::getInstance()->getSettings()->logs_plugin;
        return \Craft::$app->getView()->renderTemplate('zoho-crm-api/index', $logs_plugin, View::TEMPLATE_MODE_CP);
        /*/

        return \Craft::$app->getView()->renderTemplate('zoho-crm-api/settings', [
            'settings' => $this->getSettings()
        ]);

    }

    private function routes() {
        Event::on(UrlManager::class, UrlManager::EVENT_REGISTER_CP_URL_RULES, function(RegisterUrlRulesEvent $event) {
            $event->rules = array_merge($event->rules, [
                'zoho-crm-api/configure/index' => 'zoho-crm-api/configure/index',
            ]);
        });
    }

}