<?php

namespace shfx17\zohocrmapi\migrations;

use Craft;
use craft\db\Migration;

/**
 * Install migration.
 */
class Install extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('zoho_api_logs', [
            'id' => $this->primaryKey()->notNull(),
            'ml_number' => $this->integer()->notNull(),
            'craft_id' => $this->integer()->notNull(),
            'result_zoho' => $this->boolean()->notNull(),
            'errors_zoho' => $this->string(255)->notNull(),
            'action' => $this->string(255)->notNull(),
            'dateCreated' => $this->dateTime()->notNull(),
            'dateUpdated' => $this->dateTime()->notNull(),
            'uid' => $this->string(255)->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('zoho_api_logs');
    }
}

